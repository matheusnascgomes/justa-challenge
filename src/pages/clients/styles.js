import styled from 'styled-components';

export const Container = styled.div`
	padding-left: 8%;
	padding-right: 8%;
	padding-top: 22px;
	padding-bottom: 22px;
	display: flex;
	flex-direction: column;
`;

export const DoubleChartWrapper = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;


	@media (max-width: 700px) {
		flex-direction: column;
		/* justify-content: space-between; */
  	}

`;