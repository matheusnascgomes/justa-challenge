import React, { Component } from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

import pT from "prop-types";

import { ChartStructure } from '../styles';

export default class DefaultChart extends Component{

	static propTypes = {
		/** Should have an object literal of `period:` and `money:` in order to fill the chart*/
		initialContent: pT.arrayOf(pT.shape({
			period: pT.string,
			money: pT.string
		})).isRequired,
	}


	componentDidMount() {

		const { initialContent } = this.props;

		let chart = am4core.create("defaultChartPlace", am4charts.XYChart);

		chart.data = initialContent;
		  
		let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.ticks.template.disabled = true;
		categoryAxis.renderer.line.opacity = 0;
		categoryAxis.renderer.grid.template.disabled = true;
		categoryAxis.renderer.minGridDistance = 40;
		categoryAxis.dataFields.category = "period";
		categoryAxis.startLocation = 0.4;
		categoryAxis.endLocation = 0.6;	
		
		let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
		valueAxis.tooltip.disabled = true;
		valueAxis.renderer.line.opacity = 0;
		valueAxis.renderer.ticks.template.disabled = true;
		valueAxis.min = 0;
		
		let lineSeries = chart.series.push(new am4charts.LineSeries());
		lineSeries.dataFields.categoryX = "period";
		lineSeries.dataFields.valueY = "money";
		lineSeries.tooltipText = "Valor: {valueY.value}";
		lineSeries.fillOpacity = 0.5;
		lineSeries.tensionX = 0.8;
		lineSeries.strokeWidth = 3;
		lineSeries.propertyFields.stroke = "lineColor";
		lineSeries.propertyFields.fill = "lineColor";
		
		let bullet = lineSeries.bullets.push(new am4charts.CircleBullet());
		bullet.circle.radius = 6;
		bullet.circle.fill = am4core.color("#fff");
		bullet.circle.strokeWidth = 3;
		
		chart.cursor = new am4charts.XYCursor();
		chart.cursor.behavior = "panX";
		chart.cursor.lineX.opacity = 0;
		chart.cursor.lineY.opacity = 0;			  

	  }
	
	componentWillUnmount() {
		if (this.chart) {
			this.chart.dispose();
		}
	}
	render(){
		return <ChartStructure id="defaultChartPlace" ></ChartStructure>								
	}

}