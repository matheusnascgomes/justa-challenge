import React, { Fragment }  from 'react';
import { BrowserRouter } from 'react-router-dom';
import Routes from './routes';
import './styles/index.css';
import Header from './components/Header';


const App = () => (
    <BrowserRouter>
        <Fragment>
            <Header />
            <Routes />
        </Fragment>
    </BrowserRouter>    
);

export default App;