import styled from 'styled-components';

export const Container = styled.header`		
	padding-left: 8%;
	padding-right: 8%;
	display: flex;
	justify-content: space-between;
	align-items: center;
	height: 60px;
	background-color: #076E95;
	color: #fff;

	a{
		text-decoration: none;
	}

	@media (max-width: 700px) {
		padding-right: 8px;
		padding-left: 8px;
 	 }

`;

export const NavLeftOption = styled.div`
	display: flex;
	align-items: center;
	padding: 16px;
	margin-left: 6px; 	
	cursor: pointer;
	font-size: 15px;	
	background-color: ${ props => props.statusOption ? '#FFF' : '#076E95'};
	color: ${ props => props.statusOption ? '#076e95' : '#FFF'};

	p{
		margin-left: 4px;
	}

	:hover{
		background-color: ${ props => !props.statusOption ? '#1b7496' : '' };
	}

	@media (max-width: 700px) {
    	p{
			display: none;
		}
 	 }

`;

export const NavLeft = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	
	img{
		height: 41px;
		cursor: pointer;
	}

	@media (max-width: 700px) {
		img{
			height: 22px;
			cursor: pointer;
		}
  	}
`;

export const NavRight = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	cursor: pointer;
	padding: 8px;

	:hover{
	 	background-color: #1b7496
	}

	img{		
		border-radius: 50%;
		width: 44px;
		height: 44px;
	}
`;

export const UserInfo = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
	margin-right: 4px;
	margin-left: 4px;

	p{
		font-size: 17px;
	}

	small{
		font-size: 10px;
	}

	@media (max-width: 700px) {
   	 	display: none;
	}
`;