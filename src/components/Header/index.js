import React, { Component } from 'react';
import { Container, NavLeft, NavRight, NavLeftOption, UserInfo } from './styles';

import { withRouter, Link } from 'react-router-dom';

import MdAnalytics from 'react-ionicons/lib/MdAnalytics'
import MdArrowDropdown from 'react-ionicons/lib/MdArrowDropdown'
import MdPeople from 'react-ionicons/lib/MdPeople';

import LogoJusta from '../../asset/img/logo_justa.png';
import FakeUser from '../../asset/img/fake_user.png';

const PATH_NAME = {
	ROOT: '/',
	CLIENTS: '/clients'
}

class Header extends Component{
	
	state = {	
		menuDashboard: {
			active: false,
			iconColor: '#FFF'
		},
		menuClients: {
			active: false,
			iconColor: '#FFF'
		}
	}

	componentDidMount(){
		const { pathname } = this.props.location;
		this.changeMenuActived(pathname);				
	}

	changeMenuActived = (routeName) => {
		
		switch (routeName) {
			case PATH_NAME.ROOT:
				this.setState({ 
					menuDashboard: { iconColor: '#076E95', active: true },  
					menuClients: { iconColor: '#FFF', active: false }  

				});
				break;						
			case PATH_NAME.CLIENTS:
			this.setState({ 
				menuClients: { iconColor: '#076E95', active: true },
				menuDashboard: { iconColor: '#FFF', active: false },  

			});
				break;			
			default:
				return this.state;				
		}	
	}

	render(){		

		return(
			<Container>
				<NavLeft>			
					<img src={ LogoJusta } title="Logo justa"  alt="Logo justa" />
					<Link to={PATH_NAME.ROOT}>
						<NavLeftOption
							onClick={ () => this.changeMenuActived(PATH_NAME.ROOT) }								
							statusOption={this.state.menuDashboard.active}
							title="Dashboard"

						>				
							<MdAnalytics color={this.state.menuDashboard.iconColor} fontSize="28px" />
							<p>	Dashboard</p>
						</NavLeftOption>
					</Link>
					<Link to={PATH_NAME.CLIENTS}>
						<NavLeftOption						
							onClick={ () => this.changeMenuActived(PATH_NAME.CLIENTS) }								
							statusOption={this.state.menuClients.active}
							title="Clientes"
						>
							<MdPeople  color={this.state.menuClients.iconColor} fontSize="28px" />
							<p>Clientes</p> 
						</NavLeftOption>
					</Link>
				</NavLeft>		
				<NavRight>
					<img src={ FakeUser } title="Foto do usuário do sistema" alt="Foto do usuário do sistema" />
					<UserInfo>
						<p>Justo Top</p>
						<small>Grupo econômico </small>
					</UserInfo>
					<MdArrowDropdown color="#fff" fontSize="28px"  />
				</NavRight>
			</Container>
		)
	}
}

export default withRouter(Header);