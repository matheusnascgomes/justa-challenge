
# Justa Challenge 


**Instalação**
Projeto desenvolvido em cima  do `create-react-app` e segue o seguinte fluxo para execução em ambiente local.

Instalação das dependências:
> `npm install` ou `yarn install`

Execução do projeto:

> `npm run start` ou `yarn start`

**Deploy**
[Clicando aqui você também encontra a aplicação em ambiente de produção](https://5c5cc81a078357bef4543781--ecstatic-kare-7f452d.netlify.com/)

**Documentação**
A documentação dos componentes foi desenvolvida utilizando o [docz](https://www.docz.site/) 
e sua visualização em ambiente de desenvolvimento pode ser facilmente acessada da seguinte forma:

> `npm run docz:dev` ou `yarn docz:dev`

**Layout Desktop**

![Layout Web](https://i.imgur.com/OYgyGpd.png  "Layout Web")

  

**Layout mobile da aplicação**

  

![Layout Mobile](https://i.imgur.com/iM7DezA.png  "Layout Mobile")

  **Estilização**
		 Pelo fato de gostar e ter bastante afinidade com o flexbox + styled components optei por não utilizar tantos componentes de estilo semi-prontos, apenas utilizei o `reactstrap` (dependência com base no bootstrap) para a striped table  da tabela de *detalhes de vendas* e *detalhes dos clientes* visando garantir uma tabela semelhante ao protótipo.
		 
**Bibliotecas Utilizadas:**

- [prop-types](https://github.com/airbnb/prop-types): Para documentação de propriedades de componentes visando torná-los auto-explicativos;

- [styled-components](https://www.styled-components.com/  "styled-components"):  Para maior flexibilidade no comportamento dos estilos a depender das propriedades dos componentes;

- [@amcharts/amcharts4](https://www.amcharts.com/docs/v4/getting-started/integrations/using-react/):  Para a projeção dos gráficos de pizza e de linha
  
- [reactstrap](https://reactstrap.github.io/): Apenas utilizado para criar striped table
- [react-ionicons](https://zamarrowski.github.io/react-ionicons/): Para a utilização dos ícones do sistema

# Observações

**Redux**
	Embora tenha sido um dos pontos mencionados na proposta do desafio, não fiz a utilização do Redux pois não enxerguei uma aplicabilidade necessária na aplicação, uma vez que não temos nenhuma ação de usuário que precise implicar na evolução do state de outros componentes, no meu ponto de vista sua utilização no cenário atual apenas impactaria na clareza da arquitetura, objetividade e funcionamento do projeto. No entanto, aprecio bastante o Redux + Redux Saga, e como sua aplicabilidade em projetos mais complexo pode se tornar produtiva.

**Gráficos**
Por ter uma certa experiencia com o `amcharts` resolvi aplicá-lo por questão de afinidade e agregação de valor no curto prazo. No entanto, os gráficos com `amcharts` trabalham com temas predefinidos, o que me impediu de alcançar exatamente o mesmo estilo do protótipo. 
*Obs.: além disso o gráfico de linha no eixo Y ficou com a formatação numérica também um pouco diferente do protótipo, mas assim que identificada esta solução estarei atualizando o repositório.*