import styled from 'styled-components';

import pT from "prop-types";

export const Panel = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	width:  ${props => props.customWidth ? props.customWidth : '100%' }; 
	padding-bottom: 16px;

	box-shadow: 2px #000;

	margin-left: ${props => props.spaceOnLeft ? '12px' : '' };

	@media (max-width: 700px) {
		width: 100%;
		margin-left: 0;
  	}

`;

export const PanelHeader = styled.div`
	background-color: #076E95;
	padding-left: 8px;
	padding-top: 2px;
	padding-bottom: 2px;
	color: #FFF;	
`;

export const PanelBody = styled.div`
	height: 400px;
	background-color: #FFF;
	box-shadow: 1px 1px 2px #dedede;

	padding: ${ ({ applySomePadding }) => applySomePadding ? applySomePadding : '' };

`;

export const TableWrapper = styled.div`
	height: 100%;
	overflow-y: scroll;
`;

Panel.propTypes = {
	customWidth: pT.number,
	spaceOnLeft: pT.number
}

PanelBody.propTypes = { 
	applySomePadding: pT.number
}