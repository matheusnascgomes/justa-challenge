import styled from 'styled-components';

export const Container = styled.div`
	padding-left: 8%;
	padding-right: 8%;
	padding-top: 22px;
	padding-bottom: 22px;
	display: flex;
	flex-direction: column;
`;
