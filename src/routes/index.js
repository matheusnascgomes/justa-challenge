import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Dashbaord from '../pages/dashboard';
import Clients from '../pages/clients';

const Routes = () => (
  <Switch>
    <Route exact path="/" component={ Dashbaord }  />
    <Route exact path="/clients" component={ Clients } />
  </Switch>
);

export default Routes;