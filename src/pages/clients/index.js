import React, { Component } from 'react';

import { Table } from 'reactstrap';
import { Container, DoubleChartWrapper } from './styles';
import { Panel, PanelHeader, PanelBody, TableWrapper } from  '../../styles/global';
import DefaultChart from '../../components/Chart/DefaultChart';
import PieChart from '../../components/Chart/PieChart';

import { TRANSACTION_VOLUME, BASE_CLIENTS, CLIENT_LIST } from '../../mock/ClientConent';

export default class Clients extends Component { 

	state = { clientList: null }

	componentDidMount(){
		this.setState({ clientList: CLIENT_LIST })
	}


	render(){
		const { clientList } = this.state;

		return(
			<Container>
				<DoubleChartWrapper>
					<Panel customWidth="60%">
						<PanelHeader>
							Volume de transações
						</PanelHeader>
						<PanelBody>							
							<DefaultChart initialContent={ TRANSACTION_VOLUME } />														
						</PanelBody>
					</Panel>					
					<Panel customWidth="40%" spaceOnLeft>
						<PanelHeader>
							Base de clientes
						</PanelHeader>
						<PanelBody applySomePadding="20px">		
							<PieChart initialContent={ BASE_CLIENTS } />						
						</PanelBody>
					</Panel>
				</DoubleChartWrapper>				
				<Panel>
					<PanelHeader>
						Detalhes dos clientes
					</PanelHeader>
					<PanelBody>
						<TableWrapper>
							<Table striped>
								<thead>
									<tr>
										<th>Nome</th>
										<th>Empresa</th>
										<th>Status</th>
										<th>Última atualização</th>
										<th>Notas</th>
									</tr>
								</thead>
								<tbody>
									{ clientList && clientList.map(client => (
										<tr key={ client.id }>
											<td>{ client.name }</td>
											<td>{ client.company }</td>
											<td>{ client.status }</td>
											<td>{ client.last_update }</td>
											<td>{ client.notes }</td>
										</tr>
									))}										
								</tbody>
							</Table>
						</TableWrapper>				
					</PanelBody>
				</Panel>				
			</Container>
		)
	}
}