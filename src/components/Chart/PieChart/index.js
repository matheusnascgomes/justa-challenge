import React, { Component } from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

import { ChartStructure } from '../styles';

import pT from "prop-types";

export default class PieChart extends Component{

	static propTypes = {
		/** Should have an object literal of `title:` and `value:` in order to fill the chart*/
		initialContent: pT.arrayOf(pT.shape({
			title: pT.string,
			value: pT.number
		})).isRequired,
	}


	componentDidMount() {

		const { initialContent } = this.props;


		let chart = am4core.create("pieChartPlace", am4charts.PieChart);

		// Add data
		chart.data = initialContent;

		// Add and configure Series
		let pieSeries = chart.series.push(new am4charts.PieSeries());
		pieSeries.dataFields.value = "value";
		pieSeries.dataFields.category = "title";
		pieSeries.slices.template.stroke = am4core.color("#fff");
		pieSeries.slices.template.strokeWidth = 2;
		pieSeries.slices.template.strokeOpacity = 1;

		pieSeries.labels.template.disabled = true;
		pieSeries.ticks.template.disabled = true;

		// This creates initial animation
		pieSeries.hiddenState.properties.opacity = 1;
		pieSeries.hiddenState.properties.endAngle = -90;
		pieSeries.hiddenState.properties.startAngle = -90;
		
		// Add a legend
		chart.legend = new am4charts.Legend();

	}
	
	componentWillUnmount() {
		if (this.chart) {
			this.chart.dispose();
		}
	}
	render(){
		return <ChartStructure id="pieChartPlace" ></ChartStructure>								
	}

}