export const TRANSACTION_VOLUME = [
	{
		"period": "31/12",
		"money": "60.000,00",
	}, 
	{
		"period": "05/01",
		"money": "50.000,00",
	}, 
	{
		"period": "14/01",
		"money": "75.000,00",
	}, 
	{
		"period": "20/01",
		"money": "30.000,00",
	}, 
	{
		"period": "22/01",
		"money": "25.000,00",
	}, 
	{
		"period": "28/01",
		"money": "53.000,00",
	}
];

export const SALES =  [
	{
		id: 1,
		name: 'Eugenia King',
		company: 'Breakfast of Champions',
		status: 'Idle',
		last_update: '11/01/2017',
		notes: 'Fact and Truth'
	},
	{
		id: 2,
		name: 'Marie Schneider',
		company: 'Breakfast of Champions',
		status: 'Closed',
		last_update: '04/24/2017',
		notes: 'On Being Human'
	},
	{
		id: 3,
		name: 'Phoebe Frazier',
		company: 'Breakfast of Champions',
		status: 'Idle',
		last_update: '05/28/2017',
		notes: 'Always Look On The Bright Side Of Life'
	},
	{
		id: 4,
		name: 'Lou Walfe',
		company: 'Breakfast of Champions',
		status: 'Closed',
		last_update: '05/07/2017',
		notes: 'Fact and Truth'
	},
	{
		id: 5,
		name: 'Eugenia King',
		company: 'Breakfast of Champions',
		status: 'Actived',
		last_update: '05/24/2017',
		notes: 'Peace On Earth A Wonderful Wish But No Way'
	},
];