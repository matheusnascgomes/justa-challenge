import React, { Component } from 'react';

import { Table } from 'reactstrap';
import { Container } from './styles';
import { Panel, PanelHeader, PanelBody, TableWrapper } from  '../../styles/global';
import DefaultChart from '../../components/Chart/DefaultChart';


import { TRANSACTION_VOLUME, SALES } from '../../mock/DashBoardContent';


export default class Dashboard extends Component { 

	state = {
		sales: null
	}

	componentDidMount(){
		this.setState({ sales: SALES })
	}
	
	render(){
		const { sales } = this.state;

		return(
			<Container>
				<Panel>
					<PanelHeader>
						Volume de vendas aprovadas
					</PanelHeader>
					<PanelBody>
						<DefaultChart initialContent={ TRANSACTION_VOLUME } />				
					</PanelBody>
				</Panel>
				<Panel>
					<PanelHeader>
						Detalhes das vendas
					</PanelHeader>
					<PanelBody>
						<TableWrapper>
							<Table striped>
								<thead>
									<tr>
										<th>Nome</th>
										<th>Empresa</th>
										<th>Status</th>
										<th>Última atualização</th>
										<th>Notas</th>
									</tr>
								</thead>
								<tbody>									
									{ sales && sales.map(sale => (
										<tr key={ sale.id }>
											<td>{ sale.name }</td>
											<td>{ sale.company }</td>
											<td>{ sale.status }</td>
											<td>{ sale.last_update }</td>
											<td>{ sale.notes }</td>
										</tr>
									))}																		
								</tbody>
							</Table>
						</TableWrapper>					
					</PanelBody>
				</Panel>				
			</Container>
		)
	}
}